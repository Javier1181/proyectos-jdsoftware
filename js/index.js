 <script>
			$(function(){
				$("[data-toggle='tooltip']").tooltip();
				$(".carousel").carousel({
					interval: 2000
				});				
				$("#contacto").on("show.bs.modal",function(e){
					console.log("El modal se está mostrando");
					
					$("#contactoBtn").removeClass("btn-outline-success");
					$("#contactoBtn").addClass("btn-primary");
					$("#contactoBtn").prop("disabled",true);
				});
				$("#contacto").on("shown.bs.modal",function(e){
					console.log("El modal se ha mostrado");
				});
				$("#contacto").on("hide.bs.modal",function(e){
					console.log("El modal se está ocultando");
				});
				$("#contacto").on("hidden.bs.modal",function(e){
					console.log("El modal se oculta");
					$("#contactoBtn").removeClass("btn-primary");
					$("#contactoBtn").addClass("btn-outline-success");
					$("#contactoBtn").prop("disabled",false);
				});
				<!--$("[data-toggle='popover']").popover();-->
			});				
</script>